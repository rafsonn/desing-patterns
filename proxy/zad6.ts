interface Report {
	displayReportTemplate(reportFormat: string, reportEntries: number): void;
	generateReport(reportFormat: string, reportEntries: number): void;
	generateSensitiveReport(): void;
}

class ReportGenerator implements Report {
	displayReportTemplate(reportFormat: string, reportEntries: number): void {
		console.log(
			`ReportGenerator: Displaying blank report template in ${reportFormat} format with ${reportEntries} entries.`
		);
	}
	generateReport(reportFormat: string, reportEntries: number): void {
		console.log(
			`ReportGenerator: Generating complex report in ${reportFormat} format with ${reportEntries} entries.`
		);
	}
	generateSensitiveReport(): void {
		console.log('ReportGenerator: Generating sensitive report');
	}
}

class ReportGeneratorProxy implements Report {
	reportGenerator: Report;
	constructor(private _employee: Employee) {}
	displayReportTemplate(reportFormat: string, reportEntries: number): void {
		console.log(`[${this._employee.name}, ${this._employee.role}]:`);
		console.log(
			`ReportGeneratorProxy: Displaying blank report template in ${reportFormat} format with ${reportEntries} entries.`
		);
	}
	generateReport(reportFormat: string, reportEntries: number): void {
		if (this.reportGenerator == null) this.reportGenerator = new ReportGenerator();
		console.log(`[${this._employee.name}, ${this._employee.role}]:`);
		this.reportGenerator.generateReport(reportFormat, reportEntries);
	}
	generateSensitiveReport(): void {
		if (this._employee.role === 'Manager') {
			if (this.reportGenerator == null) this.reportGenerator = new ReportGenerator();
			console.log(`[${this._employee.name}, ${this._employee.role}]:`);
			this.reportGenerator.generateSensitiveReport();
		} else {
			console.log(`[${this._employee.name}]:`);
			console.log('Access to sensitive reports denied.');
		}
	}
}

class Employee {
	constructor(private _name: String) {}
	private _role: string;

	get name() {
		return this._name;
	}

	get role() {
		return this._role;
	}

	set role(role: string) {
		this._role = role;
	}
}

function clientCode(report: Report, entries) {
	report.displayReportTemplate('Pdf', entries);
	report.generateReport('Pdf', entries);
	report.generateSensitiveReport();
}

let employee1: Employee = new Employee('John');
employee1.role = 'Manager';

let proxy: Report = new ReportGeneratorProxy(employee1);

console.log('Client: Executing with a real subject:');
clientCode(proxy, 123);
console.log('\nPosition change\n');
employee1.role = 'Seller';
clientCode(proxy, 351);

console.log('\n\n');

console.log('Client: Executing with a proxy:');
let report: Report = new ReportGenerator();
report.displayReportTemplate('Pdf', 232);
report.generateReport('Pdf', 95);
report.generateSensitiveReport();
