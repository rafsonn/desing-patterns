//
// Rafał Klifeld 167883
//

class Keyboard {
	constructor() {
		if (Keyboard.instance instanceof Keyboard) {
			return Keyboard.instance;
		}

		this.key_pressed = '';
		this.observers = [];

		Object.seal(this);
		Keyboard.instance = this;
	}

	keyPress = (pressed) => (this.key_pressed = pressed);
	registerKey = (observer) => this.observers.push(observer);
	unregisterKey = (observer) => {
		var index = this.observers.indexOf(observer);
		if (index > -1) {
			this.observers.splice(index, 1);
		}
	};

	notifyObserver = (observer) => {
		observer.notify();
	};

	getRandomIntInclusive = (min, max) => {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	};

	generateKeypress = () => {
		this.key_pressed = `k${this.getRandomIntInclusive(1, 5)}`;
		let key = this.observers.find((x) => x.getName() === this.key_pressed);
		if (key) {
			this.unregisterKey(key);
			this.notifyObserver(key);
		}
	};

	printObservers = () => {
		let res = '';
		if (this.observers.length) {
			res = 'Register: \n';
			this.observers.forEach((observer) => (res += ` ${observer.getName()}`));
		} else {
			res = 'Register is empty.';
		}
		console.log(res);
	};
}

class Key {
	constructor(name = '') {
		this.name = name;
	}
	getName = () => this.name;
	notify = () => console.log(`Pressed ${this.getName()}`);
}

var keyboard = new Keyboard();
var keyboard2 = new Keyboard();

console.log(`keyboard is equal to keyboard2: ${keyboard === keyboard2}`);

for (let index = 1; index <= 5; index++) {
	keyboard.registerKey(new Key('k' + index));
}

keyboard.printObservers();

while (keyboard.observers.length) {
	keyboard.generateKeypress();
}

keyboard.printObservers();
