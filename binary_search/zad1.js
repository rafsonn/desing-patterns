//
// Rafał Klifeld 167883
//

arr = [ 2, 2, 4, 5, 7, 9, 10 ];

function binarySearch(arr, x) {
	if (arr.length == 0) {
		console.log('Element not found!');
		return false;
	}

	middle = Math.floor((arr.length - 1) / 2);

	if (arr[middle] === x) {
		console.log('Element found! [' + x + ']');
		return true;
	}

	if (arr[middle] > x && middle > 0) {
		binarySearch(arr.slice(0, middle), x);
	} else if (arr[middle] < x && middle !== 0) {
		binarySearch(arr.slice(-middle), x);
	} else {
		console.log('Element not found!');
	}
}

function test() {
	arr = [ 2, 2, 4, 5, 7, 9, 10 ];
	val1 = 2;
	val2 = 10;
	val3 = 1;
	val4 = 14;
	console.log(arr);
	console.log('=================');
	console.log('Value: ' + val1);
	binarySearch(arr, val1);
	console.log('-----------------');
	console.log('Value: ' + val2);
	binarySearch(arr, val2);
	console.log('-----------------');
	console.log('Value: ' + val3);
	binarySearch(arr, val3);
	console.log('-----------------');
	console.log('Value: ' + val4);
	binarySearch(arr, val4);
}

test();
