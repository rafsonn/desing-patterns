const readline = require('readline');

class Shipping {
	calculate(carrier) {
		return carrier.calculate();
	}
}

class Carrier {
	calculate() {
		let avalible = this._time <= data.time && this._cost <= data.money ? ' [AVALIBLE] ' : '[UNAVALIBLE]'
		return `${avalible} Transport by ${this._name} (risk: ${this._risk})`
	}
}

class Bike extends Carrier {
	constructor() {
		super();
		this._name = 'Bike';
		this._cost = 0;
		this._time = 60;
		this._risk = 'big';
	}
}

class PublicTransport extends Carrier {
	constructor() {
		super();
		this._name = 'Public Transport';
		this._cost = 3;
		this._time = 30;
		this._risk = 'medium';
	}
}

class Taxi extends Carrier {
	constructor() {
		super();
		this._name = 'Taxi';
		this._cost = 20;
		this._time = 15;
		this._risk = 'small';
	}
}

const data = {
	time: 0,
	money: 0
}
const shipping = new Shipping();


const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

console.log('Shipping calculator\n')
rl.question("Enter amount of avalible time [min]: ", (time) => {
	rl.question("Enter amount of avalible cash [USD]: ", (money) => {
		data.time = time
		data.money = money
		rl.close()
	});
});

rl.on('close', () => {
	console.log('\n')
	console.log(shipping.calculate(new Bike(data)));
	console.log(shipping.calculate(new PublicTransport(data)));
	console.log(shipping.calculate(new Taxi(data)));
})