class Handler {
	constructor() {
		this._successor = null;
		if (this.constructor === Handler) {
			throw new TypeError("Can not construct abstract class.");
		}
		if (this.handleRequest === Handler.prototype.handleRequest) {
			throw new TypeError("Please implement abstract method handleRequest.");
		}
	}
	set successor(successor) {
		this._successor = successor
	}
	get successor() {
		return this._successor
	}

	handleRequest = () => { throw new TypeError("Do not call abstract method handleRequest from child."); }
}

class SquareSearch extends Handler {
	handleRequest = (array) => {

		if (array) {
			let delay = 100;
			let date = new Date();
			let timer = date.setTime(date.getTime() + delay);
			let expired = false;
			function validate() {
				var now = new Date();
				if (+now > timer)
					return false;
				else
					return true;
			}

			let i = 0;
			let sum = 0;
			let maxsofar = 0;
			while (i < array.length && !expired) {
				if (!validate()) {
					console.log("Square algorithm: Time expired");
					expired = true;
				}
				i++;
				sum = 0;
				for (let j = i; j < array.length; j++) {
					sum += array[j];
					maxsofar = Math.max(maxsofar, sum);
				}
			}
			if (expired && this.successor) this.successor.handleRequest(array)
			else console.log(`Square algorithm: ${maxsofar}`);

			// return amount;
		} else {
			if (this.successor) {
				this.successor.handleRequest(array);
			}
		}
	}
}
class LinearSearch extends Handler {
	handleRequest = (array) => {
		if (array) {
			let maxsofar = 0;
			let sum = 0;
			for (let i = 0; i < array.length; i++) {
				sum = 0;
				for (let j = i; j < array.length; j++) {
					sum += array[j];
					// console.log(sum);
					maxsofar = Math.max(maxsofar, sum);
				}
			}
			console.log(`Linear algorithm: ${maxsofar}`);
		} else {
			if (this.successor) {
				this.successor.handleRequest(amount);
			}
		}
	}
}

class Cilent {
	constructor() {
		var squareSearch = new SquareSearch(),
			linearSearch = new LinearSearch();

		squareSearch.successor = linearSearch;

		this.chain = squareSearch;
	}

	serachMaxSum = (arg) => {
		this.chain.handleRequest(arg);
	}
}

let client = new Cilent();
const array = [31, -41, 59, 26, -53, 58, 97, -93, -23, 84];
const array2 = Array.from({ length: 40000 }, () => Math.floor(Math.random() * 200 - 100));

console.log(`----- ${array.length}el vector -----`)
client.serachMaxSum(array);

console.log(`---- ${array2.length}el vector ----`)
client.serachMaxSum(array2);